#!/usr/bin/env python

# How to send an web socket message using http://www.tornadoweb.org/.
# to get ready you have to install pika and tornado
# 1. pip install pika
# 2. pip install tornado
import json
import logging
from collections import defaultdict
from itertools import chain
from logging.handlers import TimedRotatingFileHandler
from threading import Thread
import pika
import tornado.ioloop
import tornado.web
import tornado.websocket
from click._compat import raw_input
from twisted.web.http import urlparse

logger = logging.getLogger('broker')
logger.setLevel(logging.INFO)

handler = TimedRotatingFileHandler('/var/log/broker.log', when='midnight', backupCount=2)
frm = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(module)s : %(lineno)d - %(message)s')
handler.setFormatter(frm)
handler.addFilter(logging.Filter('broker'))
logger.addHandler(handler)

clients = defaultdict(list)

connection = pika.BlockingConnection()
logger.info('Connected:localhost')

channel = connection.channel()


def threaded_rmq():
    channel.exchange_declare(exchange='direct', type='direct')
    channel.queue_declare(queue='queue')
    channel.queue_bind(exchange='direct', queue='queue', routing_key='/status')
    channel.queue_bind(exchange='direct', queue='queue', routing_key='/report')
    logger.info('consumer ready, on queue')
    channel.basic_consume(consumer_callback, queue="queue", no_ack=True)
    channel.start_consuming()


def disconnect_to_rabbitmq():
    channel.stop_consuming()
    connection.close()
    logger.info('Disconnected from Rabbitmq')


def consumer_callback(ch, method, properties, body):
    body = body.decode('utf-8')
    # logger.info('[x] Received from routing_key=%s -> %s' % (method.routing_key, body))
    obj = json.loads(body)
    # The messagge is brodcast to the connected clients
    for client in clients[method.routing_key]:
        client.write_message(obj)


class SocketHandler(tornado.websocket.WebSocketHandler):
    def open(self):
        logger.info('WebSocket opened -> ' + str(self.request))
        clients[self.request.path].append(self)
        # clients.append(self)

    def on_close(self):
        logger.info('WebSocket closed -> ' + str(self.request))
        clients[self.request.path].remove(self)
        # clients.remove(self)

    def on_pong(self, data):
        logger.info('WebSocket pong -> ' + str(data) + str(self.request))

    def check_origin(self, origin):
        parsed_origin = urlparse(origin.encode('utf-8'))
        origin = parsed_origin.hostname.decode('utf-8')
        origin = origin.lower()
        host = self.request.headers.get("Host").split(':')
        return origin == host[0]
        # return True


def ping():
    for c in chain(*clients.values()):
        c.ping(b'--heartbeat--')


application = tornado.web.Application([
    (r'/report', SocketHandler), (r'/status', SocketHandler),
])


def startTornado():
    task = tornado.ioloop.PeriodicCallback(
        ping,
        30 * 1000)
    task.start()
    application.listen(port=8888, address='localhost')
    #                , ssl_options={
    #     'certfile': 'server.crt',
    #     'keyfile': 'server.key'
    # })
    tornado.ioloop.IOLoop.instance().start()


def stopTornado():
    tornado.ioloop.IOLoop.instance().stop()


if __name__ == '__main__':
    logger.info('Starting thread RabbitMQ')
    threadRMQ = Thread(target=threaded_rmq)
    threadRMQ.daemon = True
    threadRMQ.start()

    logger.info('Starting thread Tornado')

    threadTornado = Thread(target=startTornado)
    #threadTornado.daemon = True
    threadTornado.start()
    try:
        raw_input('Server ready. Press enter to stop\n')
    except SyntaxError:
        pass
    try:
        logger.info('Disconnecting from RabbitMQ..')
        disconnect_to_rabbitmq()
    except Exception as e:
        pass
    stopTornado();

    logger.info('See you...')
